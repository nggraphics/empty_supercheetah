$(function() {

	gfMaxW();	
	$(window).resize(function(){gfMaxW();})

	function gfMaxW (){
		var w = $("#gf").width();
		var marginL = ($(window).width() - w)/2;
		$("#gf").css("padding-left", marginL).css("padding-right", marginL);
	}
  
});
